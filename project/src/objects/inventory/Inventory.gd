extends Node

var current_index : int# keeps track of current placeholder index
var word_object # word object (letters)

#--------------------------------------------CONSTANTS-----------------------------------------------------------

const PLACEHOLDER_OBJECT = preload("res://src/objects/inventory/InventoryPlaceholder.tscn")
const BAD_PlACEHOLDER_TEXTURE = preload("res://assets/textures/inventory/BadPlaceholder.tres")
const GOOD_PLACEHOLDER_TEXTURE = preload("res://assets/textures/inventory/GoodPlaceholder.tres")
const CURRENT_PLACEHOLDER_TEXTURE = preload("res://assets/textures/inventory/CurrentPlaceholder.tres")
const COLLECTED_PLACEHOLDER_TEXTURE = preload("res://assets/textures/inventory/CollectedPlaceholder.tres")


#-------------------------------------------MAIN FUNCTIONS------------------------------------------------------------

func _ready():
	# initial settings
	current_index = 0
	word_object = get_node("/root/Game/Level/Placeholder") # gets word placeholder object
	word_object.connect("new_letter_collected", self, "_on_new_letter_collected")

	instance_placeholder(word_object.get_letter_count())
	set_placeholder_texture(current_index, CURRENT_PLACEHOLDER_TEXTURE)


func _on_new_letter_collected(count: int):
	if(count != current_index + 1): return  # Updates only when new letter is collected

	update_placeholder(current_index)
	current_index += 1
	set_placeholder_texture(current_index, CURRENT_PLACEHOLDER_TEXTURE)

#------------------------------------------CORE-------------------------------------------------------------

func instance_placeholder(number: int):
	for i in range(number):
		var instance = PLACEHOLDER_OBJECT.instance()
		instance.get_child(0).set_text(word_object.collected[i])
		self.add_child(instance)

func update_placeholder(index: int):
	var placeholder = get_child(index).get_child(0)
	var update_texture : bool = placeholder.text == "" # if letter is not given at the start => don't update texture

	placeholder.set_text(word_object.collected[index]) #updates text field

	if(update_texture): set_placeholder_texture(index, COLLECTED_PLACEHOLDER_TEXTURE)
	elif is_correct_letter(index): set_placeholder_texture(index, GOOD_PLACEHOLDER_TEXTURE)
	else: set_placeholder_texture(index, BAD_PlACEHOLDER_TEXTURE);

func set_placeholder_texture(index: int, texture: Resource):
	if(get_child_count() > index): # if wrong index => don't update texture
		get_child(index).set("custom_styles/panel", texture)

func is_correct_letter(index: int):
	return word_object.word[index] == word_object.collected[index]

func reset_inventory():
	# just delete the old ones and recreate them
	for x in get_children():
		x.queue_free()
		self.remove_child(x)

	self._ready()
