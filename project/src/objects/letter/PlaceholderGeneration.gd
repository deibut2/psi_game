extends TileMap

const LETTER_OBJECET = preload("res://src/objects/letter/LetterObject.tscn")
var answer : String = Global.get_answer()
export var tile_set_id = 0

var player_position : Vector2
var path : Array

var rnd = RandomNumberGenerator.new()


func _ready():
	rnd.randomize()

	# gets player position from level object
	player_position = world_to_map(get_parent().player_position)

	path = bfs(player_position)
	set_placeholder_randomly()

func set_path_map(path, tile_set_id = self.tile_set_id):
	for cell in path:
		set_cellv(cell, tile_set_id)
	var rect = get_used_rect()
	update_bitmask_region(rect.position, rect.end)

func set_placeholder_randomly():
	var step = len(path) / len(answer)

	for i in range(len(answer)):
		var random_index = rnd.randi_range(i * step + 1, (i+1) * step - 1)
		var placeholder_pos = map_to_world(path[random_index]) + cell_size / 2
		var placeholder = LETTER_OBJECET.instance()

		placeholder.position = placeholder_pos
		placeholder.display_text = answer[i]

		get_parent().get_node("Placeholder").add_child(placeholder)

func direction(dir : int):
	var vector = Vector2.ZERO
	match dir:
		0: vector = Vector2.UP
		1: vector = Vector2.RIGHT
		2: vector = Vector2.DOWN
		3: vector = Vector2.LEFT
	return vector

func bfs(from: Vector2):
	var cells = get_used_cells()
	var visited = []

	var q = [from]

	while (len(q) > 0):
		var cell = q.pop_front()

		for i in range(4):
			var nextCell = cell + direction(i)
			if cells.has(nextCell) && !visited.has(nextCell):
				q.append(nextCell)
				visited.append(nextCell)
	return visited
