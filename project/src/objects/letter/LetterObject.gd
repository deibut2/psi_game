extends Area2D

var display_text = ""
var collected = false
onready var pickup_sound = $PickupSoundEffect


func render_letter():
	var object_number = int(self.name) - 1
	var letter = get_parent().get_letter(object_number)

	if letter == "":
		queue_free()
	else:
		display_text = letter

func reset_letter():
	show()
	collected = false

func _on_LetterObject_body_entered(body):
	if !collected && body.name == "Player":
		pickup_sound.volume_db = Options.sound_volume;
		pickup_sound.play()
		get_parent().add_collected(display_text)
		hide()
		collected = true
