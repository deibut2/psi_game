extends Node2D

signal new_letter_collected(count)
signal level_ended(collected)

var word = Global.get_answer()
var count = 0 # collected leter counter
var collected = PoolStringArray()

func _ready():
	set_collected_by_difficulty()

func set_collected_by_difficulty():
	collected = PoolStringArray()
	var difficulty = Global.game_difficulty
	if(difficulty == Global.GAME_DIFFICULTY.HARD):
		for _i in len(word):
			collected.append("")
	elif(difficulty == Global.GAME_DIFFICULTY.MEDIUM):
		for index in len(word):
			if(index == 0 or index == len(word)-1):
				collected.append(word[index])
			else:
				collected.append("")
	elif(difficulty == Global.GAME_DIFFICULTY.EASY):
		for letter in word:
			collected.append(letter)

# function that gets called from Letterobject when a letter is collected
func add_collected(new_value: String):
	collected[count] = new_value
	count += 1
	emit_signal("new_letter_collected", count)

	if count == len(word):
		emit_signal("level_ended", collected.join(""))

# get letter count
func get_letter_count():
	return word.length()

func reset_placeholder():
	count = 0
	for i in get_children():
		i.reset_letter()

	set_collected_by_difficulty()
