extends Node

# timerValue will count the number of seconds that passed
var timer_value = 0

# getting timer node
onready var timer = get_node("Timer")

func _ready():
	# every 1 second the timer calls timer timeout function
	timer.set_wait_time(1)
	timer.start()

# on every timer timeout function call (every second), increment timerValue by 1
func _on_Timer_timeout():
	timer_value += 1

func stop_timer():
	timer.stop()
	
func resume_timer():
	timer.start()
	
func reset_timer():
	timer_value = 0
