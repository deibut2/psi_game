extends Control

onready var move_up_button = $MainContainer/Options/Controls/Movement/MoveUp/Button
onready var move_down_button = $MainContainer/Options/Controls/Movement/MoveDown/Button
onready var move_right_button = $MainContainer/Options/Controls/Movement/MoveRight/Button
onready var move_left_button = $MainContainer/Options/Controls/Movement/MoveLeft/Button

onready var question_button = $MainContainer/Options/Controls/Question/Question/Button
onready var fullscreen_toggle = $MainContainer/Options/Controls/Window/Mode/Toggle
onready var back_button = $MainContainer/

onready var music_slider = $MainContainer/Options/Controls/Audio/Music/Slider
onready var sound_slider = $MainContainer/Options/Controls/Audio/Sound/Slider

onready var menu_music = $MenuMusic

onready var button_hover_sound = $ButtonHoverSound

onready var button_click_sound = $ButtonClickSound

func _ready():
	menu_music.volume_db = Options.music_volume;
	menu_music.play(Global.main_menu_music_playback_position)

	button_hover_sound.volume_db = Options.sound_volume;
	button_click_sound.volume_db = Options.sound_volume;
	
	music_slider.value = Options.music_volume + 50
	sound_slider.value = Options.sound_volume + 50
	
	if Global.config.has_section_key("display", "fullscreen"):
		fullscreen_toggle.pressed = Global.config.get_value("display", "fullscreen")

# option is received as true or false. true - fullscreen, false - windowed
func _on_toggle_fullscreen(option):
	OS.window_fullscreen = option
	Global.config.set_value("display", "fullscreen", option)
	Options.save_config()

func _on_BackButton_pressed():
	var playback_pos = menu_music.get_playback_position()
	Global.main_menu_music_playback_position = playback_pos

func _on_Button_hover():
	button_hover_sound.play()


func _on_music_volume_change(value):
	Options.music_volume = -50 + value;
	Global.config.set_value("volume", "music", Options.music_volume)
	Options.save_config()
	
	menu_music.volume_db = Options.music_volume;

func _on_sound_volume_change(value):
	Options.sound_volume = -50 + value;
	Global.config.set_value("volume", "sound", Options.sound_volume)
	Options.save_config()
	
	button_hover_sound.volume_db = Options.sound_volume
	button_click_sound.volume_db = Options.sound_volume
