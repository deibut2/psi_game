
extends Button

var waiting_input = false

onready var button_click_sound = $"/root/OptionsMenuUI/ButtonClickSound"

func name_to_control():
	match get_parent().name:
		"MoveLeft": return "move_left"
		"MoveRight": return "move_right"
		"MoveUp": return "move_up"
		"MoveDown": return "move_down"
		"Question": return "ui_question"
		_: return ""

func _ready():
	text = Options.parse_action(name_to_control())
	button_click_sound.volume_db = Options.sound_volume;


func _input(event):
	if waiting_input:
		if event is InputEventJoypadMotion:
			if abs(event.axis_value) < 0.2: # ignore slight stick movements
				return
		if event is InputEventMouseMotion: # ignore mouse
			return
		if event is InputEventMouseButton or (event is InputEventKey and event.scancode == KEY_ESCAPE): # ignore mouse and escape
			pressed = false
			waiting_input = false

			text = Options.parse_action(name_to_control())
			return
		if event is InputEvent:
			pressed = false
			waiting_input = false

			Options.add_control(name_to_control(), event)
			text = Options.parse_action(name_to_control())

func _toggled(button_pressed):
	if button_pressed:
		button_click_sound.play()
		waiting_input = true
		set_text("Press Any Key")
