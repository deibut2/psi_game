extends Button

export var reference_path = ""
export(bool) var start_focused = false
export(bool) var change_scene = true

onready var button_click_sound = $ButtonClickSound
onready var button_hover_sound = $ButtonHoverSound

func _ready():
	if(start_focused):
		grab_focus()

	connect("mouse_entered",self,"_on_Button_mouse_entered")
	connect("pressed",self,"_on_Button_Pressed")

func _on_Button_mouse_entered():
	grab_focus()

func _on_Button_Pressed():
	if Global.GAME_DIFFICULTY.values().has(self.name):
		Global.game_difficulty = self.name
		Global.random_question()
	if(reference_path != "" and change_scene):
		get_tree().paused = false
		get_tree().change_scene(reference_path)
	elif(reference_path == "" and change_scene):
		get_tree().quit()

func _on_MeniuButton_button_down():
	button_click_sound.volume_db = Options.sound_volume;
	button_click_sound.play()

func _on_MeniuButton_hover():
	button_hover_sound.volume_db = Options.sound_volume;
	button_hover_sound.play()
