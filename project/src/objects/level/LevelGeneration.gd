extends TileMap

# border around screen (unit -> number of tiles)
export var outline : int = 2 

# default map size (this is minimum screen size)
export var size : Vector2 = Vector2(9, 16) # calculation: 9 * 2 - 1 = 17 (this is done because algorithm works better on odd numbers)

export var tile_set_id :int = 0

onready var pathways = []
var borders : Rect2
var rnd = RandomNumberGenerator.new()

func _ready():
	rnd.randomize()
	
	# Map resize based on answer lenght
	var answer_lenght = len(Global.get_answer()) / 4
	size += Vector2(answer_lenght, answer_lenght)

	# Creates and fills the map  
	var cells = set_wall_map(size.x, size.y, tile_set_id)
	
	# counts border of map for level generation
	borders = get_borders(outline)
	
	# Generates pats and them removes them from wall tile map
	pathways = generate_level()
	remove_cells(pathways)


func set_wall_map(height, width, tile_set_id = self.tile_set_id):
	for x in range(width * 2 - 1):
		for y in range(height * 2 -1):
			set_cell(x,y,tile_set_id)

func remove_cells(path):
	for cell in path:
		set_cellv(cell, -1)
	var rect = get_used_rect()
	update_bitmask_region(rect.position, rect.end)

func get_borders(outline = 1):
	var rect = get_used_rect()
	rect.position += Vector2.ONE * outline
	rect.end -= Vector2.ONE * 2 * outline
	return rect

func generate_level():
	var maze = []
	for x in range(borders.position.x, borders.end.x, 2):
		for y in range(borders.position.y, borders.end.y, 2):
			var cell = Vector2(x,y)
			maze.append(cell)
			if !borders.has_point(cell): continue
			
			var neighbours = get_neighbours(cell)
			if neighbours.empty(): continue
			
			neighbours.shuffle()
			var neighbour = neighbours.pop_front()
			
			maze.append(neighbour)
	return maze

func get_neighbours(cell):
	var neighbours = []
	for point in [Vector2.LEFT, Vector2.UP]:
		if borders.has_point(cell + point):
			neighbours.append(cell + point)
	return neighbours
