extends Node

export var player_position = Vector2.ZERO
onready var placeholder = $Placeholder

const WALL_TILE_MAP_OBJECT = preload("res://src/objects/level/WallTileMap.tscn")
const PATH_TILE_MAP_OBJECT = preload("res://src/objects/level/PathTileMap.tscn")

var rnd = RandomNumberGenerator.new()

func _ready():
	rnd.randomize()
	var random_tile_set_id = rnd.randi_range(0, 1)
	
	# Adds new generated wall map to level object
	var wall_tile_set = WALL_TILE_MAP_OBJECT.instance()
	wall_tile_set.tile_set_id = random_tile_set_id # for level texture change
	self.add_child(wall_tile_set)
	
	# Setting player spawn position randomly
	wall_tile_set.pathways.shuffle() 
	var cell = wall_tile_set.pathways.front()
	player_position = (cell * Global.TILE_SIZE)
	player_position += Vector2(Global.TILE_SIZE / 2, Global.TILE_SIZE / 2) # this line needed for aligment to grid
	
	# Adds path map to level object
	var path_tile_set = PATH_TILE_MAP_OBJECT.instance()
	path_tile_set.tile_set_id = random_tile_set_id # for level texture change
	path_tile_set.set_path_map(wall_tile_set.pathways) # getting wall map deleted cells and applying then to create path map
	self.add_child(path_tile_set)


func reset_level():
	$Placeholder.reset_placeholder()
