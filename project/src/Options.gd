extends Node

var sound_volume = 0
var music_volume = 0

func add_control(action, input):
	if is_binded(action, input):
		unbind_key(action, input)
	else:
		InputMap.action_add_event(action, input)
		Global.config.set_value("controls", action, InputMap.get_action_list(action))
		Options.save_config()

func is_binded(curr_action, input):
	for action in ["move_left", "move_right", "move_up", "move_down", "ui_question"]:
		if InputMap.action_has_event(action, input): return true
	return false

func unbind_key(action, input):
	if InputMap.action_has_event(action, input):
		InputMap.action_erase_event(action, input)
		Global.config.erase_section_key("controls", action);
		Options.save_config()		

func convert_controller_input_to_text(number):
	match number:
		JOY_BUTTON_0: return "Button 0"
		JOY_BUTTON_1: return "Button 1"
		JOY_BUTTON_2: return "Button 2"
		JOY_BUTTON_3: return "Button 3"
		JOY_BUTTON_4: return "Button 4"
		JOY_BUTTON_5: return "Button 5"
		JOY_BUTTON_6: return "Button 6"
		JOY_BUTTON_7: return "Button 7"
		JOY_BUTTON_8: return "Button 8"
		JOY_BUTTON_9: return "Button 9"
		JOY_BUTTON_10: return "Button 10"
		JOY_BUTTON_11: return "Button 11"
		JOY_BUTTON_12: return "Button 12"
		JOY_BUTTON_13: return "Button 13"
		JOY_BUTTON_14: return "Button 14"
		JOY_BUTTON_15: return "Button 15"
		JOY_BUTTON_16: return "Button 16"
		JOY_BUTTON_17: return "Button 17"
		JOY_BUTTON_18: return "Button 18"
		JOY_BUTTON_19: return "Button 19"
		JOY_BUTTON_20: return "Button 20"
		JOY_BUTTON_21: return "Button 21"
		_: return "Unknown"

func convert_controller_axis_to_text(axis, axis_value):
	if (axis_value > 0):
		match axis:
			JOY_AXIS_0: return "Left stick right"
			JOY_AXIS_1: return "Left stick down"
			JOY_AXIS_2: return "Right stick right"
			JOY_AXIS_3: return "Right stick down"
			_: return "Unknown"
	else:
		match axis:
			JOY_AXIS_0: return "Left stick left"
			JOY_AXIS_1: return "Left stick up"
			JOY_AXIS_2: return "Right stick left"
			JOY_AXIS_3: return "Right stick up"
			_: return "Unknown"

func parse_action(action):
	var ret = ""
	var empty = true;

	for input in InputMap.get_action_list(action):
		if !empty:
			ret += ", "
		if input is InputEventKey:
			ret += OS.get_scancode_string(input.get_scancode_with_modifiers())
		if input is InputEventJoypadButton:
			ret += Options.convert_controller_input_to_text(input.button_index)
		if input is InputEventJoypadMotion:
			ret += Options.convert_controller_axis_to_text(input.axis, input.axis_value)
		
		empty = false
	return ret

func load_config():
	var err = Global.config.load("user://settings.cfg")
	if err == OK: # If not, something went wrong with the file loading
		if Global.config.has_section_key("display", "fullscreen"):
			var fullscreen = Global.config.get_value("display", "fullscreen")
			OS.window_fullscreen = fullscreen
		
		if Global.config.has_section_key("volume", "music"):
			music_volume = Global.config.get_value("volume", "music");
		if Global.config.has_section_key("volume", "sound"):
			sound_volume = Global.config.get_value("volume", "sound");

		var inputs = ["move_left", "move_right", "move_up", "move_down", "ui_question"]
		for input in inputs:
			if Global.config.has_section_key("controls", input):
				InputMap.action_erase_events(input)
				var controls = Global.config.get_value("controls", input)

				for control in controls:
					InputMap.action_add_event(input, control)

func save_config():
	Global.config.save("user://settings.cfg")
