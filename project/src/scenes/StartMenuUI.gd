extends Control

onready var menu_music = $MenuMusic

func _ready():
	menu_music.volume_db = Options.music_volume;

	menu_music.play(Global.main_menu_music_playback_position)

func _on_Menu_Button_pressed():
	var playback_pos = menu_music.get_playback_position()
	Global.main_menu_music_playback_position = playback_pos
