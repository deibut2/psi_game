extends Node

const LEVELS_COUNT = 2
const TILE_SIZE = 32
const GAME_DIFFICULTY = {
	EASY = "Easy",
	MEDIUM = "Medium",
	HARD = "Hard",
	NONE = "None"
}

onready var questions: Array = load_question_json("res://src/Questions.json")
var current_quesion_id = 0

var game_difficulty = GAME_DIFFICULTY.NONE

var config = ConfigFile.new()

var main_menu_music_playback_position = 0

func _ready():
	randomize()
	random_question()
	Options.load_config()

func load_question_json(path):
	var file = File.new()
	file.open(path, File.READ)
	var text = file.get_as_text()
	file.close()
	return parse_json(text)
	
func random_question():
	current_quesion_id = randi() % questions.size()
	
func get_question(id = current_quesion_id):
	return questions[id].question
	
func get_answer(id = current_quesion_id):
	return questions[id].answer.to_upper()
