extends KinematicBody2D

onready var tween = $Tween
onready var ray = $RayCast2D
onready var movement_sound = $MovementSoundEffect

export var tile_size = 32
export var speed = 7

var steps = 0

var inputs = {
	"move_right": [Vector2.RIGHT, 90],
	"move_left": [Vector2.LEFT, -90],
	"move_up": [Vector2.UP, 0],
	"move_down": [Vector2.DOWN, 180]
}

func _ready():
	#--------------------SCALE SETTINGS---------------------------------
	var sprite_size = $Sprite.texture.get_size()
	$CollisionShape2D.shape.extents = Vector2(tile_size, tile_size) / 2
	$Sprite.scale = Vector2(tile_size, tile_size) / sprite_size
	#-------------------------------------------------------------------

	position = position.snapped(Vector2.ONE * tile_size)
	position += Vector2.ONE * tile_size/2

func _physics_process(_delta):
	movement_sound.volume_db = Options.sound_volume;
	if tween.is_active():
		return
	for direction in inputs.keys():
		if Input.is_action_pressed(direction):
			move(direction)

func rotate(direction):
	$Sprite.rotation_degrees = inputs[direction][1]

func move(direction):
	ray.cast_to = inputs[direction][0] * tile_size
	ray.force_raycast_update()
	if !ray.is_colliding():
		rotate(direction)
		move_tween(direction)
		steps+=1

func move_tween(direction):
	movement_sound.play()
	movement_sound.pitch_scale = rand_range(0.5, 1)
	tween.interpolate_property(self, "position",
		position, position + inputs[direction][0] * tile_size,
		1.0/speed, Tween.TRANS_SINE, Tween.EASE_OUT)
	tween.start()
