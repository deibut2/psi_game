extends Node2D

onready var player = $Player
onready var timer = $UI/GameUI/GameTimer

onready var game_music = $GameMusic
onready var menu_music = $MenuMusic

func _ready():
	set_question(Global.get_question())
	game_music.volume_db = Options.music_volume;
	menu_music.volume_db = Options.music_volume;

	# set palyer position and camera constraints
	player.position = $Level.player_position
	set_camera_constraints()
	$UI/StartQuestion/VBoxContainer/MarginContainer/BackToGameButton.grab_focus()

	# Connects level ended signal to game script
	$Level.placeholder.connect("level_ended", self, "_on_level_ended")

func _physics_process(_delta):
	if Input.is_action_just_pressed("ui_pause"):
		if get_tree().paused == false:
			_on_MenuButton_pressed()
		elif !$UI/QuestionUI.visible:
			_on_Resume_pressed()
	elif Input.is_action_just_pressed("ui_question"):
		if get_tree().paused == false:
			_on_QuestionButton_pressed()
		elif $UI/QuestionUI.visible:
			_on_BackToGameButton_pressed()

func set_question(question):
	$UI/QuestionUI/VBoxContainer/QuestionText.text = question

func set_camera_constraints():
	var level_size = $Level/WallTileMap.get_used_rect()
	var camera = $Player/Camera2D

	camera.limit_top = level_size.position[1] * Global.TILE_SIZE
	camera.limit_left = level_size.position[0] * Global.TILE_SIZE
	camera.limit_right = level_size.end[0] * Global.TILE_SIZE
	camera.limit_bottom = level_size.end[1] * Global.TILE_SIZE

func toggle_ui(ui_name):
	var is_visible = ui_name.visible
	for child in $UI.get_children():
		if(child == ui_name):
			child.visible = !is_visible
			continue
		child.visible = is_visible

func _on_level_ended(collected):
	$UI/LevelEndedUI/Container/ScrollContainer/StatsContainer/QuestionContainer/Queston.text = Global.get_question()
	$UI/LevelEndedUI/Container/ScrollContainer/StatsContainer/AnswerContainer/Answer.text = collected
	var timer_string = $UI/GameUI/GameTimer/RichTextLabel.format_time(timer.timer_value, 1 << 0 | 1 << 1 | 1 << 2)
	$UI/LevelEndedUI/Container/ScrollContainer/StatsContainer/StatisticContainer/TimeContainer/Time.text = timer_string

	$UI/LevelEndedUI/Container/ScrollContainer/StatsContainer/StatisticContainer/StepContainer/Steps.text = str(player.steps)

	toggle_ui($UI/LevelEndedUI)
	$UI/LevelEndedUI/Container/ButtonsContainer/NextLevel.grab_focus()
	get_tree().paused = true

	game_music.stop()
	menu_music.stream_paused = false

func _on_MenuButton_pressed():
	toggle_ui($UI/PauseMenu)
	$UI/PauseMenu/ButtonsContainer/Resume.grab_focus()
	get_tree().paused = true

	game_music.stream_paused = true
	menu_music.stream_paused = false

func _on_Resume_pressed():
	toggle_ui($UI/GameUI)
	get_tree().paused = false

	menu_music.stream_paused = true
	game_music.stream_paused = false

func _on_Restart_pressed():
	$Level.reset_level()
	toggle_ui($UI/GameUI)
	$UI.get_node("GameUI").get_node("GameTimer").reset_timer()
	$UI.get_node("GameUI").get_node("Inventory").reset_inventory()

	remove_child(player) # remove it so the name "Player" frees up so the letter pick-uping works
	player.queue_free() # actually clear the memory
	player = load("res://src/player/PlayerObject.tscn").instance() # remake the player object
	add_child(player)
	player.position = $Level.player_position # set it to the correct position
	set_camera_constraints() # and fix the camera
	get_tree().paused = false

	menu_music.play()
	menu_music.stream_paused = true
	game_music.play()
	game_music.stream_paused = false

func _on_QuestionButton_pressed():
	toggle_ui($UI/QuestionUI)
	$UI/QuestionUI/VBoxContainer/MarginContainer/BackToGameButton.grab_focus()
	get_tree().paused = true

	game_music.stream_paused = true
	menu_music.stream_paused = false

func _on_BackToGameButton_pressed():
	toggle_ui($UI/GameUI)
	get_tree().paused = false

	menu_music.stream_paused = true
	game_music.stream_paused = false

func _on_StartGameButton_pressed():
	# inventory reset is required on map load to make Inventory UI work
	# when selecting a random map
	$UI.get_node("GameUI").get_node("Inventory").reset_inventory()

	$UI/StartQuestion.queue_free()
	toggle_ui($UI/GameUI)
	get_tree().paused = false

	menu_music.play()
	menu_music.stream_paused = true
	game_music.play()

func _on_StartQuestion_ready():
	get_tree().paused = true
	$UI/StartQuestion/VBoxContainer/QuestionText.text = Global.get_question()

func _on_NextLevel_pressed():
	Global.random_question()
	get_tree().reload_current_scene()

	menu_music.play()
	menu_music.stream_paused = true
	game_music.play()
	game_music.stream_paused = false


func _on_MainMenu_pressed():
	game_music.stop()
