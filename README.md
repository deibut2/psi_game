## Projekto idėjos aprašymas

Žaidimo “A Book of Everything” pasaulyje, nuniokotame milžiniškos kometos smūgio, per plauką išgyvenusi žmonija prarado visą išsaugotą kaitmeninę ir fizinę informaciją, tuo tarpu ir vieną iš svarbiausių jos šaltinių - knygas.  

Žaidėjo užduotis yra sukurti enciklopediją apie viską, ieškant naujosios enciklopedijos žodžių tarp kometos smūgio sukurtų nuolaužų labirinto. Šis žaidimas padeda lavinti problemų sprendimo įgūdžius, sužinoti naujų faktų apie įvairias temas ir pasitikrinti jau turimas žinias.  

Žaidimo pradžioje atsitiktinai parenkamas klausimas, kurio atsakymo raidės yra išmėtytos labirinte. Valdant žaidimo veikėją, privaloma iš eilės surinkti šias raides, kad būtų sėkmingai užbaigtas vienas žaidimo lygis. Pagal pasirinktą sudėtingumo lygmenį pilnas klausimo atsakymas gali būti iškart suteikiamas žaidėjui, suteikiamos tik pirma ir paskutinės raidės arba nesuteikiama nei viena žodžio raidė.  
